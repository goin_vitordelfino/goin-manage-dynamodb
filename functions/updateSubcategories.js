const fs = require('fs');

const arrayA = JSON.parse(fs.readFileSync('./files/converted/baladas.xlsx.json', 'utf8'));
const arrayB = JSON.parse(fs.readFileSync('./files/converted/treated/baladas___.xlsx.json', 'utf8'));


arrayA.forEach((itemA, index) => {
    if(itemA['SERTANEJO'] == 'TRUE') arrayB[index].clubs.subcategories.push('bf192ddd-28b8-425c-9aab-6b2915a02527')
    if(itemA['ROCK'] == 'TRUE') arrayB[index].clubs.subcategories.push('42bdb4fe-7ff2-48c7-9ee6-a9d6e5bdb0ae')
    if(itemA['POP'] == 'TRUE') arrayB[index].clubs.subcategories.push('f5f569d9-d1f2-40c4-9cdf-9aaaa99ab22a')
    if(itemA['ALTERNATIVAS'] == 'TRUE') arrayB[index].clubs.subcategories.push('ee8d509d-7076-4c64-9dfb-621d089839c8')
    if(itemA['BLACK'] == 'TRUE') arrayB[index].clubs.subcategories.push('a62ba550-921b-4e6f-b2d4-d0db0ec42f57')
    if(itemA['BLUES_JAZZ'] == 'TRUE') arrayB[index].clubs.subcategories.push('ca943421-4d64-49c6-8409-16365903eb7b')
    if(itemA['ELETRONICA'] == 'TRUE') arrayB[index].clubs.subcategories.push('36a80cb5-b52e-48d1-a66c-16c0bc07f1d2')
    if(itemA['LGBT'] == 'TRUE') arrayB[index].clubs.subcategories.push('3a085070-7306-4945-9834-387201054cce')
    if(itemA['LATINA'] == 'TRUE') arrayB[index].clubs.subcategories.push('7cc7ebca-bdfe-4df7-b3be-daeedb177d68')
    if(itemA['SAMBA'] == 'TRUE') arrayB[index].clubs.subcategories.push('39560ecc-c7ae-4f18-9c85-c85fc35372cc')
    if(itemA['PAGODE_MPB'] == 'TRUE') arrayB[index].clubs.subcategories.push('48b54f94-fc7d-47d7-920a-5d350d6e7e6b')
    if(itemA['FUNK'] == 'TRUE') arrayB[index].clubs.subcategories.push('e5874e31-59f1-4ce3-b90d-8ba6967a700d')
    if(itemA['FORRO'] == 'TRUE') arrayB[index].clubs.subcategories.push('a0b2f443-5d9a-454f-9e01-100f43abeb74')
    if(itemA['OUTROS'] == 'TRUE') arrayB[index].clubs.subcategories.push('52f74c98-7d92-4f5d-84c5-5b8c5ce62286')

    //console.log(`${ itemA['NOME'] } - ${ arrayB[index].companyName }`)

})

fs.writeFile(`./files/converted/treated/baladas.json`, JSON.stringify(arrayB), (err) => {
    if(err) {
        console.log('Erro', err.message);
    } else {
        console.log('File saved');
    }
});



