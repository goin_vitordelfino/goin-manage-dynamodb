const bcrypt = require('bcrypt');
const saltRounds = 10;
const myPlaintextPassword = '123456';

const hash = bcrypt.hashSync(myPlaintextPassword, saltRounds);

console.log(hash);

unhash = bcrypt.compareSync('123456', hash);

console.log(unhash);

