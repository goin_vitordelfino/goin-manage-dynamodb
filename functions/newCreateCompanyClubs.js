"use strict";

require('cross-fetch/polyfill');
const amazonCognito = require('amazon-cognito-identity-js');
const fs = require('fs');
const AWS = require('aws-sdk');


const userPool = new amazonCognito.CognitoUserPool({
    UserPoolId: 'us-east-1_ypCBmuyLK',
    ClientId: '306u74uoamdn92jkuvcgeb875k',
});

main();

const createCognitoUser = (username, userType) => new amazonCognito.CognitoUser({
    Username: username,
    Pool: userPool
});

const createCognitoCredentials = (username, password) => new amazonCognito.AuthenticationDetails({
    Username: username,
    Password: password
});

const signInCognito = (username, password, userType) => new Promise((resolve, reject) => {
    const authenticationDetails = createCognitoCredentials(username, password);
    const cognitoUser = createCognitoUser(username, userType);
    cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess(signinResult) {
            console.log('onSucess');
            //resolve(cognitoUser);
            resolve(signinResult);

        },
        onFailure(signinErr) {
            console.log('onFailure');
            reject(signinErr);
        }
    });
});

function setAmazonCredentialsParam(token)  {
    const loginOptions = {}
    loginOptions['cognito-idp.us-east-1.amazonaws.com/us-east-1_ypCBmuyLK'] = token;
    AWS.config.region = 'us-east-1';
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'us-east-1:8272cd20-c87a-4cd9-8a2b-d97d5c2eafe1',
        Logins: loginOptions
    });
};

async function getAmazonCredentials() {
    return new Promise((resolve, reject) => {
        AWS.config.getCredentials((err) => {
            if(err) {
               console.log('getAmazonCredentials Error: ', err.message);
               return reject(err);
            }
            return resolve(AWS.config.credentials);
        });
    });
};

function main() {

    const array = JSON.parse(fs.readFileSync('./files/converted/treated/baladas.json','utf8'));

    for(let company of array) {

        let email = company.clubs.email ?
            company.clubs.email :
            (company.companyName.replace(/ /g, '').replace(/[^\w\s]/gi, '') + '@goinapp.com.br').toLocaleLowerCase();

        let password = 'b931b52b';

        console.log('email: ' + email)

        userPool.signUp(email, password, null, null, (err, result) => {
            if(err) {
                console.log(err.message);
            }

            console.log('username: ' + result.user.getUsername());

            signInCognito(result.user.getUsername(), password, 'Admin')
                .then(async (cognitoUserSession) => {
                    console.log('cognitoUser', cognitoUserSession.idToken.jwtToken);
                    setAmazonCredentialsParam(cognitoUserSession.idToken.jwtToken);
                    getAmazonCredentials()
                        .then(credentials => {
                            console.log('credentials', credentials.identityId)
                        })
                        .catch(err => console.log('err credentials', err.message))
                })
                .catch(errSignIn => console.log('signInCognito Error:  ' + errSignIn.message));
        });
    }
}

