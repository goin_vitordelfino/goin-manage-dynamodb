const NodeGeocoder = require('node-geocoder');

const options = {
  provider: 'google',
 
  // Optional depending on the providers
  httpAdapter: 'https', // Default
  apiKey: 'AIzaSyAloLhX-HBAO1xMWilMI24FuhLeLhabpCY', // for Mapquest, OpenCage, Google Premier
  formatter: null         // 'gpx', 'string', ...
};

module.exports = class GeoLocation {  
  
  constructor(number, street, city) {
    this.number = number,
    this.street = street,
    this.city = city

    this.geocoder = NodeGeocoder(options);
  }

  async getLatLong() {

    return new Promise((resolve, reject) => {
      return this.geocoder
        .geocode(`${this.street}, ${this.number} - ${this.city}`)
          .then(result => resolve(result))
          .catch(err => reject(err));
    
    });
  }

}