const AWS = require('aws-sdk');

AWS.config.update({ 
    region: 'sa-east-1',
    endpoint: new AWS.Endpoint('http://localhost:8000')
});

const dyn = new AWS.DynamoDB();

const params = {
    TableName: 'GoInAdmin',
    KeySchema: [{ AttributeName: 'id', KeyType: 'HASH'  }],
    AttributeDefinitions: [{ AttributeName: 'id', AttributeType: 'S' }],
    ProvisionedThroughput: {   
        ReadCapacityUnits: 5,
        WriteCapacityUnits: 5
    }
};

dyn.createTable(params, (err, data) => {
    if (err) {
        console.log('Error', err);
    } else {
        console.log('Success', data);
    }
});