const AWS = require('aws-sdk');
const credentials = new AWS.SharedIniFileCredentials({ profile: 'prod' });
AWS.config.credentials = credentials;
const cognitoIdentityService = new AWS.CognitoIdentityServiceProvider({
    apiVersion: '2016-04-19',
    region: 'us-east-1'
})

const params = {
    'UserPoolId': 'us-east-1_OnQZQ1uEo'
}

cognitoIdentityService.listUsers(params, (err, data) => {
    if(!err) {
        console.log('Seccess...');
        console.log(JSON.stringify(data));
    }else {
        console.log('Error...');
        console.log(err.message);
    }
})