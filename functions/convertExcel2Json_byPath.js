const fs = require('fs');
const converter = require('xls-to-json');
const Club = require('../models/clubs.js')
const Company = require('../models/company.js');
const GeoLocation = require('./getGeoLocation.js');

const pathToRead = './files/xls';
const pathToWrite = './files/converted';

main();

async function main() {

    fs.readdir(pathToRead, async (err,files) => {
        if(!err) {

            for(let file of files) {

                await convertFile(`${pathToRead}/${file}`,`${pathToWrite}/${file}.json`,'DIGITACAO')
                    .then(async result => {

                        console.log(`${file} converted.`);

                        let companys = [];
                        
                        for(let item of result) {

                            console.log(item.NOME);
                            
                            await new GeoLocation(item.NUM, item.END, item.CIDADE)
                                .getLatLong()
                                    .then(gl => {
                                        console.log(`latitude: ${gl[0].latitude} , longiture: ${gl[0].longitude}`);
                                        companys.push(new Company(null,item.EMAIL || null, item.NOME || null,
                                            item.NOME || null,null,item.TELEFONE || null,item.CEP || null,
                                            item.END || null,item.NUM || null,null,item.CIDADE || null,
                                            null,null,
                                            new Club(null, null,
                                                item.NOME || null,item.EMAIL || null,
                                                item.DESCRICAO || null,item.SITE || null,
                                                item.TELEFONE || null,item.END || null,
                                                item.CIDADE || null,null,
                                                '0',null,null,null,null,
                                                null,null,
                                                gl[0].latitude,
                                                gl[0].longitude
                                            ),
                                            null
                                        ));
                                    })
                                    .catch(err => console.log(err.message));                       
                            
                        }                
                
                        fs.writeFile(`${pathToWrite}/treated/${file}.json`, JSON.stringify(companys), (err) => {
                            if(err) {
                                console.log('Error to save treated file', err.message);
                            } else {
                                console.log('File saved');
                            }
                        });

                    }).catch(err => console.log(`Error to convert ${file}`, err.message));                
            }
        }else {
            console.log(`Error to read path`, err.message);
        }
    });
}

async function convertFile(input, output, sheet) {
    return new Promise((resolve, reject) => {
        converter({ 
            input, output, sheet
         }, (err, result) => {
            if(!err) {
                resolve(result)
            }else {
                reject(err)
            }
         })
    })
}
