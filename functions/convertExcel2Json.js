/* import { Company } from './models/company';
import { Club } from './models/clubs';
 */
const Company = require('../models/company.js');
const Club = require('../models/clubs.js');
const converter = require('xls-to-json');
const fs = require('fs');

converter({
    input: '../files/DIGITACAO.xlsx',
    output: '../files/converted.json',
    sheet: 'DIGITACAO'
},(err, result) => {

    let companys = [];
    if(err) {
        console.log('error', err);
    } else {
        
        result.forEach(item => {
            companys.push(new Company(
                null,
                item.EMAIL || null,
                item.NOME || null,
                item.NOME || null,
                null,
                item.TELEFONE || null,
                item.CEP || null,
                item.END || null,
                item.NUM || null,
                null,
                item.CIDADE || null,
                null,
                null,
                new Club(
                    null, 
                    null,
                    item.NOME || null,
                    item.EMAIL || null,
                    item.DESCRICAO || null,
                    item.SITE || null,
                    item.TELEFONE || null,
                    item.END || null,
                    item.CIDADE || null,
                    null,
                    '0',
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ),
                null
            ));

        });


        fs.writeFile(`../files/converted/modeled/${file}.json`, JSON.stringify(companys), (err) => {
            if(err) {
                console.log('Erro', err.message);
            } else {
                console.log('File saved');
            }
        });
    };
});