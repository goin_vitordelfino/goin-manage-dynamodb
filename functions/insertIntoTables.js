const AWS = require('aws-sdk');
const uuid = require('uuid/v4');
const fs = require('fs');
const request = require('request');

const path = './files/converted/treated';

const credentials = new AWS.SharedIniFileCredentials({profile: 'prod'});

AWS.config.credentials = credentials;

AWS.config.update({ 
    region: 'sa-east-1',
    // endpoint: new AWS.Endpoint('http://localhost:8000'),
    convertEmptyValues: true
});

const dyn = new AWS.DynamoDB();

main();

async function main() {


    fs.readdir(path, async (err, files) => {
        if(!err) {

            for(let file of files) {
                console.log(`Reading file: ${file} `);
                let companys = JSON.parse(fs.readFileSync(`${path}/${file}`, 'utf8'));

                for(let company of companys) {
                    console.log(`\t Creating Cognito to: ${company.companyName}`);

                    let email = company.clubs.email ? 
                        company.clubs.email : 
                        (company.companyName.replace(/ /g, '').replace(/[^\w\s]/gi, '') + '@goinapp.com.br').toLocaleLowerCase();

                    company.email = email;
                    company.clubs.email = email;

                    console.log(`\t Email: ${ company.email }`);

                    await createUser(company)
                        .then(companyId => {
                            companyId = `us-east-1:${companyId}`;
                            insertCompany(company, companyId)
                                .then(() => {
                                    insertClub(company, companyId);
                                }).catch(errCompany => console.log('\tError to insert company', errCompany.message));
                        }).catch(errCreateUser => console.log(`\tError to create user`, errCreateUser));
                }
            }
            
        }else {
            console.log(`Error to read path:`, err.message);
        }
    });

    async function createUser(company) {
        const data = JSON.stringify({
            'email': company.email,
            'password': 'b931b52b'//uuid().split('-')[0]
        });

        const url = 'http://localhost:3000/create-cognito-user'; /* 'https://xce3c6bvpl.execute-api.sa-east-1.amazonaws.com/prod/create-cognito-user'; */
        return new Promise((resolve, reject) => {
            request.post({
                headers: {
                    'Content-Type': 'application/json',
                    'identity-provider': 'cognito-idp.us-east-1.amazonaws.com/us-east-1_ypCBmuyLK'
                },
                url,
                body: data
            }, (err, response, body) => {
                if(err || response.statusCode != 200) {
                    reject(err);
                }else {
                    
                    console.log(`\tUser created with id: ${JSON.parse(body).id}`);
                    resolve(JSON.parse(body).id);
                }
            });
        });
    }
}
function insertCompany(company, companyId) {

    console.log(`\t\tinserting company`);
    const companyItem = {
        TableName: 'GoInAdmin',
        Item: {
            'id': { S: companyId },
            'email': { S: company.email },
            'tradeName': { S: company.tradeName },
            'companyName': { S: company.companyName },
            'cnpj': { S: company.cnpj },
            'phone': { S: company.phone },
            'zipCode': { S: company.zipCode },
            'address': { S: company.address },
            'addressNumber': { S: company.addressNumber },
            'addressComplement': { S: company.addressComplement },
            'city': { S: company.city },
            'state': { S: company.state },
            'companyStatus': { S: company.companyStatus },
            'image': { S: company.image }
        }
    };

    return new Promise((resolve, reject) => {
        dyn.putItem(removeBlankObjects(companyItem), (err, data) => {
            if (err) {
                reject(err);
            } else {
                console.log(`\t\tSuccess to insert company: ${company.companyName}`);
                resolve();
            }
        }); 
    });

}

function insertClub(company, companyId) {

    console.log(`\t\tinserting club`);

    const clubItem = {

        TableName: 'GoInClubs',
            Item: {
                'id': { S: uuid() },
                'companyId': { S: companyId  },
                'name': { S: company.clubs.name },
                'email': { S: company.clubs.email },
                'description': { S: company.clubs.description },
                'website': { S: company.clubs.website },
                'phone': { S: company.clubs.phone },
                'address': { S: company.clubs.address },
                'city': { S: company.clubs.city },
                'state': { S: company.clubs.state },
                'followersCount': { N: company.clubs.followersCount },
                'subcategories': { SS: company.clubs.subcategories  },
                'logoImage': { S: company.clubs.logoImage },
                'coverImage': { S: company.clubs.coverImage },
                'rating': { S: company.clubs.rating },
                'ratingCount': { S: company.clubs.ratingCount },
                'latitude': { N: company.clubs.latitude.toString() },
                'longitude': { N: company.clubs.longitude.toString() }
            }
    }

    dyn.putItem(removeBlankObjects(clubItem), (err) => {
        if (err) {
            console.log('\t\tError to insert Club', err.message);
            return false;
        } else {
            console.log(`\t\tSuccess to insert club: ${company.clubs.name}`);
            console.log(`-------------------------------------------------------`);
            return;
        }
    });

}



function removeBlankObjects(obj) {
    Object.keys(obj.Item)
       .forEach(key => {
           Object.keys(obj.Item[key]).forEach(x => {
               if(obj.Item[key][x] === '' || obj.Item[key][x] === null || obj.Item[key][x] === undefined)
                delete obj.Item[key];
           });
       });
    return obj;
}