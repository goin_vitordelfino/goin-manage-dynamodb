module.exports = class Company {

    constructor(
        id,
        email,
        tradeName,
        companyName,
        cnpj,
        phone,
        zipCode,
        address,
        addressNumber,
        addressComplement,
        city,
        state,
        companyStatus,
        clubs,
        image    
      ) {
          this.id = id;
          this.email = email;
          this.tradeName = tradeName;
          this.companyName = companyName;
          this.cnpj = cnpj;
          this.phone = phone;
          this.zipCode = zipCode;
          this.address = address;
          this.addressNumber = addressNumber;
          this.addressComplement = addressComplement;
          this.city = city;
          this.state = state;
          this.companyStatus = companyStatus;
          this.clubs = clubs;
          this.image = image;
      }
}