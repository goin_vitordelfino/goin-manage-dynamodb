module.exports = class Club {

    constructor(
        id, 
        companyId,
        name,
        email,
        description,
        website,
        phone,
        address, 
        city,
        state,
        followersCount,
        subcategories,
        logoImage,  
        coverImage,  
        followedByMe, 
        rating,  
        ratingCount,  
        latitude,  
        longitude,  
    ) {
        this.id = id
        this.companyId = companyId
        this.name = name
        this.email = email
        this.description = description
        this.website = website
        this.phone = phone
        this.address = address
        this.city = city
        this.state = state
        this.followersCount = followersCount
        this.subcategories = subcategories
        this.logoImage = logoImage
        this.coverImage = coverImage
        this.rating = rating
        this.ratingCount = ratingCount
        this.latitude = latitude
        this.longitude = longitude
        this.followedByMe = followedByMe
    }
}