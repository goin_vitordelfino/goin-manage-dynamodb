const fs = require('fs');

let array = JSON.parse(fs.readFileSync(`./files/converted/treated/teatros.xlsx.json`, 'utf8'));

array.forEach(element => {
    element.clubs.subcategories = ['163c71f7-686c-41b7-aa8a-07227470f556']
});

fs.writeFile('./files/converted/treated/teatros___.xlsx.json', JSON.stringify(array), err => console.log(err));